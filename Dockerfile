###############################################################################
# Base image for all *production* images to build upon.
#
# If you change the version here, update pyproject.toml and the version used in
# CI. We explicitly specify the platform here so that it matches the platform
# used in deployment.
FROM --platform=linux/amd64 registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.13-slim AS base

# Some performance and disk-usage optimisations for Python within a docker container.
ENV PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECODE=1

# Do everything relative to /usr/src/app which is where we install our
# application.
WORKDIR /usr/src/app

# Pretty much everything from here on needs poetry.
ARG POETRY_VERSION=1.7.1
RUN pip install --no-cache-dir poetry==$POETRY_VERSION

###############################################################################
# Install requirements.
FROM base AS installed-deps

COPY pyproject.toml poetry.lock ./

# The apt-get commands are workaround for xmlsec not yet having wheels. Only required for apps using
# SAML authentication.
RUN set -e; \
  poetry export --format=requirements.txt --output=.tmp-requirements.txt; \
  pip install --no-cache-dir -r .tmp-requirements.txt; \
  rm .tmp-requirements.txt

###############################################################################
# The last target in the file is the "default" one. In our case it is the
# production image.
#
# KEEP THIS AS THE FINAL TARGET OR ELSE DEPLOYMENTS WILL BREAK.
FROM installed-deps AS production

# The production target includes the application code.
COPY . .

ENTRYPOINT ["python", "-m", "gitlabbot"]
