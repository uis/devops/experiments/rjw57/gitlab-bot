import logging
import os
import sys

import structlog
from gidgetlab.aiohttp import GitLabBot

LOG = structlog.get_logger()

BOT = GitLabBot(os.environ["GL_BOT_USER_ID"])


@BOT.router.register("Note Hook")
async def note_event(event, gl, *args, **kwargs):
    """Whenever a comment is posted which mentions the bot, reply."""
    note = event.object_attributes.get("note")
    issue = event.data.get("issue")
    LOG.info("Note event", note=note, issue=issue)

    # Ignore empty comments or ones which are not made on issues.
    if note is None or issue is None:
        return

    # Ignore comments which do not mention us *at the beginning*.
    if not note.strip().startswith("@friendly-bot "):
        return

    LOG.info("Posting reply")
    url = f"/projects/{event.project_id}/issues/{issue['iid']}/notes"
    message = f"Thanks for commenting, @{event.data['user']['username']}!"
    await gl.post(url, data={"body": message})


def main():
    shared_processors = [
        structlog.contextvars.merge_contextvars,
        structlog.processors.add_log_level,
        structlog.processors.StackInfoRenderer(),
    ]
    if sys.stderr.isatty():
        # Pretty printing when we run in a terminal session.
        # Automatically prints pretty tracebacks when "rich" is installed
        processors = shared_processors + [
            structlog.dev.set_exc_info,
            structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M:%S", utc=False),
            structlog.dev.ConsoleRenderer(),
        ]
    else:
        # Print JSON when we run, e.g., in a Docker container.
        processors = shared_processors + [
            structlog.processors.JSONRenderer(),
        ]

    structlog.configure(
        processors=processors,
        wrapper_class=structlog.make_filtering_bound_logger(logging.NOTSET),
        context_class=dict,
        logger_factory=structlog.PrintLoggerFactory(),
        cache_logger_on_first_use=False,
    )

    BOT.run(host=os.environ.get("HOST", "0.0.0.0"), port=int(os.environ.get("PORT", "8080")))
