# Example GitLab bot

This is an example *reactive* GitLab bot using the [gidgetlab](https://pypi.org/project/gidgetlab/)
library.

## Configuration

The gidgetlab library defines the following environment variables:

* `GL_URL` - base URL of the GitLab instance
* `GL_ACCESS_TOKEN` - API token used to make GitLab requests
* `GL_SECRET` - shared secret used to validate incoming webhook requests

Additionally we add the following environment variable configuration:

* `GL_BOT_USER_ID` - numeric user id of the bot
